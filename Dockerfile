FROM debian:bullseye-slim
ARG BUILDARCH

ENV GITHUB_ACCOUNT_URL= \
    GITHUB_RUNNER_TOKEN= \
    BUILDARCH=$BUILDARCH

RUN apt update \
    && apt install -y curl

RUN mkdir actions-runner
WORKDIR /actions-runner

RUN curl -o actions-runner-linux-${BUILDARCH}-2.294.0.tar.gz -L https://github.com/actions/runner/releases/download/v2.294.0/actions-runner-linux-${BUILDARCH}-2.294.0.tar.gz
# RUN echo "98c34d401105b83906fd988c184b96d1891eaa1b28856020211fee4a9c30bc2b  actions-runner-linux-${BUILDARCH}-2.294.0.tar.gz" | shasum -a 256 -c
RUN printenv && tar xzf ./actions-runner-linux-${BUILDARCH}-2.294.0.tar.gz

CMD ./config.sh --url $GITHUB_ACCOUNT_URL --token $GITHUB_RUNNER_TOKEN