#!/usr/bin/env bash

if [ "$GITHUB_ACCOUNT_URL" == "" ]; then
    echo "Please set GITHUB_ACCOUNT_URL in your environement"
    exit 1
fi

if [ "$GITHUB_RUNNER_TOKEN" == "" ]; then
    echo "Please set GITHUB_RUNNER_TOKEN in your environement"
    exit 1
fi

echo "Environment variables seems to be set."
exit 0